PLANTEAMIENTO

He pensado usar dos métodos diferentes para las descargas de las imágenes y las frases: el método descargarImagenes() y el método descargarFrases(). Ambos métodos descargan un fichero de texto asíncronamente utilizando RestClient() con FileAsyncHttpResponseHandler().

Una vez descargados, el método descargarImagenes() lee, a través del método leer(), y almacena cada uno de los enlaces obtenidos del fichero de texto "imagenes.txt" descargado del Servidor, y llama al método contadorImagenes() para que Picasso descargue cada una de las imágenes referenciadas en los enlaces, y vuelve a llamar indefinidamente al método contadorImagenes() por cada una, tras un periodo de tiempo obtenido del fichero "intervalo.txt" dentro de la carpeta "raw".

Al mismo tiempo, el método descargarFrases() descarga y lee el fichero "frases.txt", utilizando también el método leer(), almacena las frases y seguidamente llama al método contadorFrases() para que muestre cada una de ellas indefinidamente en el mismo intervalo de tiempo.

Además, por cada error que se produce en la aplicación, se suben los errores al Servidor llamando al método subirError().

MEJORAS

Por cada llamada a los métodos contadorImagenes() y contadorFrases() se van almacenando en un ArrayList<CountDownTimer>() cada una de las instancias de los CountDownTimer que se van creando; de modo que al salir de la aplicación, al pausarla o cada vez que se hace click en el botón btnDescargar, se llama al método pararAplicacion() que cancela todos los CountDownTimer() que se han creado al llamar a los métodos contadorImagenes() y contadorFrases() desde el último instanciado hasta el primero. Esto evita que varios hilos de ejecución muestren a la vez y a destiempo las imágenes y las frases.

Además, este método, inicializa los contadores de posición de las imágenes y las frases almacenadas para que comiencen desde cero.

NOTAS

1. He colocado en Picasso una imagen con una flecha hacia abajo que indica que se está descargando la imagen del Servidor. Una vez se ha descargado la imagen correspondiente, no vuelve a aparecer esta imagen con la flecha, a no ser que la imagen que se quiere descargar no se encuentre en el Servidor e intente descargarla. En cuyo caso, cuando aparece la imagen con la flecha hacia abajo, se tiene en cuenta el tiempo del timeout de Picasso mientras se intenta descargar una imagen del Servidor. Esta imagen con una flecha hacia abajo NO se encuentra en el Servidor, sino dentro de la aplicación.

2. Si no tiene éxito la descarga de una imagen del Servidor, he colocado en Picasso una imagen con una X que aparece cuando ha habido un error en la descarga y, por tanto, la imagen correspondiente no se ha descargado. Esta imagen con una X NO se encuentra en el Servidor, sino dentro de la aplicación.